# Docker image to manually build xv
# Build:   $ docker build -t registry.gitlab.com/lilacashes/xv:build .
# Publish: $ docker push registry.gitlab.com/lilacashes/xv:build
# Run:     $ docker run -it registry.gitlab.com/lilacashes/xv:build
FROM ubuntu:20.04

WORKDIR /tmp
COPY . /tmp

RUN apt-get -y update
RUN apt-get -y install gcc make patch
RUN apt-get -y install libxt-dev libtiff-dev libjpeg-dev

RUN tar zxf zlib-1.2.11.tar.gz
RUN tar jxf libpng-1.2.54.tar.bz2

RUN tar zxf xv-3.10a.tar.gz
RUN tar zxf xv-3.10a-jumbo-patches-20050501.tar.gz
RUN gzip -d xv-3.10a-jumbo20050501-1.diff.gz
WORKDIR xv-3.10a
RUN patch -p1 < ../xv-3.10a-jumbo-fix-patch-20050410.txt
RUN patch -p1 < ../xv-3.10a-jumbo-enh-patch-20050501.txt
RUN patch -p1 < ../xv-3.10a-jumbo20050501-1.diff
RUN sed -i s/75/95/g xvjpeg.c
WORKDIR /tmp

CMD /bin/bash
